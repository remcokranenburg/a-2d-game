CC=g++
CFLAGS=-std=gnu++11 $(shell pkg-config --cflags gtkmm-3.0)
LDFLAGS=-std=gnu++11 $(shell pkg-config --libs gtkmm-3.0)
BUILD_DIR=build
INSTALL_DIR=/usr/local
PROGRAM_NAME=a-2d-game

.PHONY: all clean install uninstall

all: $(BUILD_DIR)/bin/$(PROGRAM_NAME)

clean:
	-rm -r $(BUILD_DIR);
	find -name *.o -type f -delete

install: \
		all \
		$(BUILD_DIR)/share/$(PROGRAM_NAME)/ui/main.ui \
		$(NULL)
	cp -r $(BUILD_DIR)/* $(INSTALL_DIR);

uninstall:
	-rm $(INSTALL_DIR)/bin/$(PROGRAM_NAME);
	-rm -r $(INSTALL_DIR)/share/$(PROGRAM_NAME);

# dependencies

$(BUILD_DIR)/bin/$(PROGRAM_NAME): \
		src/main.o \
		src/MyDrawingArea.o \
		src/Object.o \
		src/Ship.o \
		$(NULL)
	mkdir -p $(BUILD_DIR)/bin;
	$(CC) $(LDFLAGS) -o $@ $^;

MyDrawingArea.o: MyDrawingArea.cpp MyDrawingArea.h
	$(CC) $(CFLAGS) -c -o $@ $<;

Object.o: Object.cpp Object.h
	$(CC) $(CFLAGS) -c -o $@ $<;

Ship.o: Ship.cpp Ship.h
	$(CC) $(CFLAGS) -c -o $@ $<;

%.o: %.cpp
	$(CC) $(CFLAGS) -c -o $@ $^;

$(BUILD_DIR)/share/$(PROGRAM_NAME)/ui/main.ui: ui/main.ui
	mkdir -p $(BUILD_DIR)/share/$(PROGRAM_NAME)/ui;
	cp $^ $@
