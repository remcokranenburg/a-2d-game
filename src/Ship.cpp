#include "Ship.h"

Ship::Ship(int x, int y)
  : Object(x, y, 16) {};

Ship::~Ship() {}

void Ship::draw(const Cairo::RefPtr<Cairo::Context>& context) {
  context->save();
  context->set_line_width(2.0);
  context->set_source_rgb(0.2, 0.2, 0.9);
  context->arc(x, y, radius, 0, M_PI * 2);
  context->stroke();
  context->restore();
}
