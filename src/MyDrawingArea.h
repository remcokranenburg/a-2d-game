#ifndef MY_DRAWING_AREA_H
#define MY_DRAWING_AREA_H

#include <gtkmm.h>

class MyDrawingArea : public Gtk::DrawingArea {
public:
  MyDrawingArea(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
  virtual ~MyDrawingArea();

protected:
  virtual bool  on_draw(const Cairo::RefPtr<Cairo::Context>& context);
  bool          on_timeout();

  uint64_t      frames;
  uint64_t      frames_last_second;
  uint64_t      fps;
  uint64_t      last_second;
};

#endif // MY_DRAWING_AREA_H
