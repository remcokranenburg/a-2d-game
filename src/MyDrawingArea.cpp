#include <sstream>
#include <time.h>
#include <cairomm/context.h>
#include "MyDrawingArea.h"
#include "Ship.h"

MyDrawingArea::MyDrawingArea(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder)
  : Gtk::DrawingArea(cobject),
    frames(1),
    frames_last_second(0),
    fps(1),
    last_second(0) {
  Glib::signal_timeout().connect(sigc::mem_fun(*this, &MyDrawingArea::on_timeout), 16);
}

MyDrawingArea::~MyDrawingArea() {}

bool MyDrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context>& context) {
  const auto allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();

  // coordinates for the center of the window
  const int xc = width / 2;
  const int yc = height / 2;

  // get time
  struct timespec current_time;
  clock_gettime(CLOCK_MONOTONIC, &current_time);
  const uint64_t current_nanos = current_time.tv_nsec + 1000000000ull * current_time.tv_sec;
  const uint64_t time_diff = current_nanos - last_second;

  if(time_diff > 500000000ull) {
    fps = (frames - frames_last_second) * 2;
    frames_last_second = frames;
    last_second = current_nanos;
  }

  context->set_line_width(10.0);

  // draw red lines out from the center of the window
  context->set_source_rgb(0.8, 0.0, 0.0);
  context->move_to(0, 0);
  context->line_to(xc, yc);
  context->line_to(0, height);
  context->move_to(xc, yc);
  context->line_to(width, yc);
  context->stroke();

  // draw ship
  auto ship = Ship(32, height / 2);
  ship.draw(context);

  // draw text

  std::string message = "Hello, world!";
  std::ostringstream frames_msg, ftime_msg, fps_msg;
  frames_msg << "Frame: " << frames;
  ftime_msg << "Frame time (ms): " << 1000.0 / fps;
  fps_msg << "FPS: " << fps;

  context->move_to(width / 2 + 50, height / 2 - 200);
  context->show_text(message);

  context->move_to(width / 2 + 50, height / 2 - 150);
  context->show_text(frames_msg.str());

  context->move_to(width / 2 + 50, height / 2 - 100);
  context->show_text(ftime_msg.str());

  context->move_to(width / 2 + 50, height / 2 - 50);
  context->show_text(fps_msg.str());

  frames++;
  return true;
}

bool MyDrawingArea::on_timeout() {
  const auto allocation = get_allocation();
  auto window = get_window();

  if(window) {
    Gdk::Rectangle rectangle(0, 0, allocation.get_width(), allocation.get_height());
    window->invalidate_rect(rectangle, false);
  }

  return true;
}
