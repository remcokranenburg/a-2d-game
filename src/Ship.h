#ifndef SHIP_H
#define SHIP_H

#include "Object.h"

class Ship : public Object {
public:
  Ship(int x, int y);
  virtual ~Ship();
  virtual void draw(const Cairo::RefPtr<Cairo::Context>& context);
};

#endif // SHIP_H
