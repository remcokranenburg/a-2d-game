#include <gtkmm.h>

#include "MyDrawingArea.h"

int main(int argc, char **argv) {
  auto app = Gtk::Application::create(argc, argv, "nl.burgsoft.a-2d-game");
  auto builder = Gtk::Builder::create_from_file("../ui/main.ui");

  // derive drawingarea
  MyDrawingArea* drawingarea = 0;
  builder->get_widget_derived("drawingarea", drawingarea);

  Gtk::Window* window = 0;
  builder->get_widget("mainwindow", window);

  return app->run(*window);
}
