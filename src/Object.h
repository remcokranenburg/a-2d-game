#ifndef OBJECT_H
#define OBJECT_H

#include <cairomm/context.h>

class Object {
public:
  Object(int x, int y, int radius);
  virtual ~Object();
  void virtual draw(const Cairo::RefPtr<Cairo::Context>& context) = 0;

protected:
  int x;
  int y;
  int radius;
};

#endif // OBJECT_H
